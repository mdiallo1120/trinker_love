const { push } = require("./people")

module.exports = {
    title: function (){
        return `
$$$$$$$$\\        $$\\           $$\\                           $$\\                               
\\__$$  __|       \\__|          $$ |                          $$ |                              
   $$ | $$$$$$\\  $$\\ $$$$$$$\\  $$ |  $$\\  $$$$$$\\   $$$$$$\\  $$ | $$$$$$\\ $$\\    $$\\  $$$$$$\\  
   $$ |$$  __$$\\ $$ |$$  __$$\\ $$ | $$  |$$  __$$\\ $$  __$$\\ $$ |$$  __$$\\\\$$\\  $$  |$$  __$$\\ 
   $$ |$$ |  \\__|$$ |$$ |  $$ |$$$$$$  / $$$$$$$$ |$$ |  \\__|$$ |$$ /  $$ |\\$$\\$$  / $$$$$$$$ |
   $$ |$$ |      $$ |$$ |  $$ |$$  _$$<  $$   ____|$$ |      $$ |$$ |  $$ | \\$$$  /  $$   ____|
   $$ |$$ |      $$ |$$ |  $$ |$$ | \\$$\\ \\$$$$$$$\\ $$ |$$\\   $$ |\\$$$$$$  |  \\$  /   \\$$$$$$$\\ 
   \\__|\\__|      \\__|\\__|  \\__|\\__|  \\__| \\_______|\\__|\\__|  \\__| \\______/    \\_/     \\_______|
================================================================================================
   `.yellow
    },

    line: function(title = "="){
        return title.padStart(48, "=").padEnd(96, "=").bgBrightYellow.black
    },

    allMale: function(p){
        return p.filter  (person => person.gender === "Male")
    },

    allFemale: function(p){
        return p.filter  (person => person.gender === "Female")
    },

    nbOfMale: function(p){
        return this.allMale(p).length;
    },

    nbOfFemale: function(p){
        return this.allFemale(p).length;
    },

    nbOfMaleInterest: function(p){
        return p.filter  (person => person.looking_for === "M").length;
    },

    nbOfFemaleInterest: function(p){
        return p.filter  (person => person.looking_for === "F").length;
    },


    salaire: function(p){
        return p.filter  (person => parseInt(person.income.substring(1) )> 2000).length;
    },
    likeDrama: function(p){
        return p.filter  (person => person.pref_movie.includes("Drama") ).length;
    },

    likeFiction: function(p){
        return p.filter  (person => person.pref_movie.includes("Sci-Fi") && person.gender === "Female" ).length;
    },
    
    DocPlus1482: function(p){
        return p.filter  (person => person.pref_movie.includes("Documentary") && parseInt(person.income.substring(1) )> 1482 ).length;
    },

    NameId4000: function(p){
        const list=[]
        let nom = p.filter  (person => parseInt(person.income.substring(1) )> 4000);
            nom.forEach(element =>{
                list.push(element.id +" "+ element.first_name +" "+ element.last_name +" "+element.income)
            });
            return list
    },


    hommeRiche: function(p){
        let max = 0;
        let rich;
        this.allMale(p).forEach(person => {
            let income = parseFloat(person.income.substring(1));
            if(income > max) {
                max = income;
                rich = person; 
            }
        }); 
        return max + ' ' + rich.id + ' ' + rich.first_name + " " + rich.last_name;
    },

    salaireMoyen: function(p){
        let tabl=[];
        let id=[]
        const arraySort = p.filter((prefDocument=> parseFloat(prefDocument.income.substring(1))>0 ))

            const fai=arraySort.forEach(element => {
                tabl.push(parseFloat(element.income.substring(1)))

                })
            let red= (acc,cur)=> acc+cur

            const nbOfid=arraySort.forEach(element=>{
                    id.push(element.id)
            })
            let reverage= tabl.reduce(red)/id.length
            return  tabl.reduce(red)+ "/" +id.length+" "+ reverage + "$"
        },

    salaireMedian:  function(p) {
        const tablemedian = p.sort((a, b) => parseFloat(b.income.substring(1)) - parseFloat(a.income.substring(1)));
        let mean;
        if(p.length % 2 == 0){
            let income1 = parseFloat(p[p.length / 2].income.substring(1))
            let income2 = parseFloat(p[(p.length / 2) - 1].income.substring(1))
             mean = (income1 + income2) / 2;
        } else {
            mean = parseFloat(p[p.length / 2].income.substring(1));
        }
        return mean + " €";
     },

        Nordhem: function(p){        
         const Ov = p.filter (person => (person.latitude)> 0 ).length;
         return Ov;
    },


    salaireDuSud: function(p){
        // let tab1=[];
        // let id=[];
        // const sud=p.filter((salaire)=>salaire.latitude<0 );

        // sud.forEach(element => {
        //     tab1.push(parseInt(element.income.substring(1)))

        // });
        // sud.forEach(element=>{
        //     id.push(parseInt(element.id))
        // })

        // let reduc=(acc,rec)=>acc+rec

        // let reverage = tab1.reduce(reduc) / id.length
        
        // return reverage

        let sum_salaire = 0

        let nombrePersonne = 0

        for( let i = 0; i < p.length; i++){
            if(p[i].latitude < 0){
                 sum_salaire += parseFloat(p[i].income.substring(1))
                 nombrePersonne++
        }
        }

          return sum_salaire/nombrePersonne 


    },
            


    procheDeBerenice: function(p){
       
        // const personeOne = []
        // const allPerson = []

        // p.map(function(e){

        //     if(e.first_name === "Bérénice" && e.last_name === "Cawt"){
        //         personeOne.push(e)
        //     }else{

        //         allPerson.push(e)
        //     }

        // })

        //     return personeOne

        Cawt = p.filter(cawt =>  cawt.last_name == "Cawt");
            x = p.filter(cawt =>  cawt.last_name !== "Cawt");
            //console.log(Cawt[0].longitude);
            //console.log(x)
    
             function toRad(Value) {
                /** Converts numeric degrees to radians */
                return Value * Math.PI / 180;
            }
            
            function haversine(lat1,lat2,lng1,lng2){
                rad = 6372.8; // for km Use 3961 for miles
                deltaLat = toRad(lat2-lat1);
                deltaLng = toRad(lng2-lng1);
                lat1 = toRad(lat1);
                lat2 = toRad(lat2);
                a = Math.sin(deltaLat/2) * Math.sin(deltaLat/2) + Math.sin(deltaLng/2) * Math.sin(deltaLng/2) * Math.cos(lat1) * Math.cos(lat2); 
                c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
                return  rad * c;
    
            }
    /*
    perpigen 42.6990086 ,2.8344715
    montpellier  43.6100109, 3.8391499
            console.log(haversine(42.6990086,43.6100109,2.8344715,3.8391499));*/
    
            function calculate(p){
                let per;
                var result = haversine(Cawt[0].latitude,((Cawt[0].latitude) + 90),Cawt[0].longitude,((Cawt[0].longitude) + 90));
                    for (var i=0;i<x.length;i++){ 
                    var ans = haversine(Cawt[0].latitude,x[i].latitude,Cawt[0].longitude,x[i].longitude);
                    if (ans < result){//nearest 
                        result = ans;
                        per = x[i];
                        
                    }       
                }
                return `"Distance entree eux " ${result.toFixed(2)}, ${per.first_name} ${per.last_name} ${per.id}` ;
            }
    
            return calculate()


            

        },


    
        procheDeRui: function(p){
        const personeRui = []
        const rui = []

        // p.map(function(e){

        //     if(e.first_name === "Ruì" && e.last_name === "Brach"){
        //         personeRui.push(e)
        //     }else{

        //         rui.push(e)
        //     }

        // })

        // return personeRui   
        Ruì = p.filter(Ruì =>  Ruì.last_name == "Ruì");
        x = p.filter(Ruì =>  Ruì.last_name !== "Ruì");
        

         function toRad(Value) {
            
            return Value * Math.PI / 180;
        }
        
        function haversine(lat1,lat2,lng1,lng2){
            rad = 6372.8; 
            deltaLat = toRad(lat2-lat1);
            deltaLng = toRad(lng2-lng1);
            lat1 = toRad(lat1);
            lat2 = toRad(lat2);
            a = Math.sin(deltaLat/2) * Math.sin(deltaLat/2) + Math.sin(deltaLng/2) * Math.sin(deltaLng/2) * Math.cos(lat1) * Math.cos(lat2); 
            c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
            return  rad * c;

        }


        function calculate(){
            let per;
            var result = haversine(Cawt[0].latitude,((Cawt[0].latitude) + 90),Cawt[0].longitude,((Cawt[0].longitude) + 90));
                for (var i=0;i<x.length;i++){ 
                var ans = haversine(Cawt[0].latitude,x[i].latitude,Cawt[0].longitude,x[i].longitude);
                if (ans < result){
                    result = ans;
                    per = x[i];
                    
                }       
            }
            return `"Distance entree eux " ${result.toFixed(2)}, ${per.first_name} ${per.last_name} ${per.id}` ;
        }

        return calculate()
    

    },







    match: function(p){
        return "not implemented".red;
    }

   


 }